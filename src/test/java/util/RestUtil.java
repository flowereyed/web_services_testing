package util;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestUtil {

    public static Response getResponse() {
        return RestAssured.when()
                .get(GlobalConstants.URI)
                .andReturn();
    }

    public static ResponseEntity<String> getRestTemplate() {
        return new RestTemplate()
                .getForEntity(GlobalConstants.BASE_URI + GlobalConstants.URI, String.class);
    }
}
