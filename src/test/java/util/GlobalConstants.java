package util;

public class GlobalConstants {

    public static final String BASE_URI = "https://jsonplaceholder.typicode.com";
    public static final String URI = "/posts";
    public static final String CONTENT_TYPE_HEADER = "Content-Type";
    public static final String CONTENT_TYPE_RESPONSE_ASSURED = "application/json; charset=utf-8";
    public static final String CONTENT_TYPE_RESPONSE_TEMPLATE = "application/json;charset=utf-8";
    public static final String JSON_KEY = "userId";

    public static final int STATUS_OK = 200;
}
