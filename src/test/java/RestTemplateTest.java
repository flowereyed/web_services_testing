import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.GlobalConstants;
import util.RestUtil;

public class RestTemplateTest {

    ResponseEntity<String> response;

    @Test
    public void checkStatusCode() {
        response = RestUtil.getRestTemplate();
        Assert.assertEquals(response.getStatusCodeValue(), GlobalConstants.STATUS_OK);
    }

    @Test
    public void checkResponseHeader() {
        response = RestUtil.getRestTemplate();
        HttpHeaders headers = response.getHeaders();
        String contentTypeValue = headers.getContentType().toString();
        Assert.assertEquals(contentTypeValue, GlobalConstants.CONTENT_TYPE_RESPONSE_TEMPLATE);
    }

    @Test
    public void checkResponseBody() {
        response = RestUtil.getRestTemplate();
        String bodyAsString = response.getBody();
        Assert.assertEquals(bodyAsString.contains(GlobalConstants.JSON_KEY), true);
    }
}
