import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import util.GlobalConstants;
import util.RestUtil;

public class RestAssuredTest {

    Response response;

    @BeforeTest
    public void initTest() {
        RestAssured.baseURI = GlobalConstants.BASE_URI;
    }

    @Test
    public void checkStatusCode() {
        response = RestUtil.getResponse();
        Assert.assertEquals(response.getStatusCode(), GlobalConstants.STATUS_OK);
    }

    @Test
    public void checkResponseHeader() {
        response = RestUtil.getResponse();
        String responseContentTypeHeader = response.getHeader(GlobalConstants.CONTENT_TYPE_HEADER);
        Assert.assertEquals(responseContentTypeHeader, GlobalConstants.CONTENT_TYPE_RESPONSE_ASSURED);
    }

    @Test
    public void checkResponseBody() {
        response = RestUtil.getResponse();
        ResponseBody responseBody = response.getBody();
        String bodyAsString = responseBody.asString();
        Assert.assertEquals(bodyAsString.contains(GlobalConstants.JSON_KEY), true);
    }
}
